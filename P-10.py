def is_prime(n):
  for i in range(2,round(n**0.5)+1):
    if n % i == 0:
      return False
  return True
sum = 0
for i in range(2,2000000):
  if is_prime(i):
    sum += i
print(sum)
