def no_of_divisors(n):
  return len([i for i in range(1,n+1) if n % i == 0])

def consecutive_positive_divisors(LIMIT):
  return len([n for n in range(LIMIT) if no_of_divisors(n) == no_of_divisors(n+1)])

print(consecutive_positive_divisors(int(pow(10,7))))
