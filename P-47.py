def is_prime(n):
  if n in [2,3,5,7]:
    return True
  if n % 2 == 0:
    return False
  r = 3
  while r * r <= n:
    if n % r == 0:
      return False
    r += 2
  return True

def prime_factors(n):
  return [i for i in range(2,n//2) if n % i == 0 and is_prime(i) == True]
  
def consecutive_distinct_primes(LIMIT):
  return [(n,n+1,n+2) for n in range(2,LIMIT) if len(prime_factors(n)) == len(prime_factors(n+1)) == len(prime_factors(n+2)) == 3]

print(consecutive_distinct_primes(1000))
