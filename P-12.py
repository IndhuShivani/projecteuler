def no_of_divisors(n):
  return len([i for i in range(1,n+1) if n % i == 0])
def triangular_list(LIMIT):
  return [(n * (n -1))//2 for n in range(2,LIMIT)]
def highly_divisible_triangular_number(LIMIT):
  return [i for i in range(LIMIT) if i in triangular_list(LIMIT//5) and no_of_divisors(i) > 5]
print(highly_divisible_triangular_number(100)[0])
