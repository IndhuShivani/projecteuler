CUBES = []
def cubic_permutations():
  i = 0
  while True:
    cube = sorted(list(str(i**3)))
    CUBES.append(cube)
    if CUBES.count(cube) == 5:
      return pow((CUBES.index(cube)),3)
      break
    i += 1
print(cubic_permutations())
