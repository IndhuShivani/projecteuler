def alexandrian():
  return sorted(set(p * q * r for p in range(-LIMIT,0) for q in range(-LIMIT,0) for r in range(0,LIMIT) if p * q + q * r + r * p == 1))[15000]
print(alexandrian(100000000))

