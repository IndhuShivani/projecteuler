
from itertools import islice 

def is_prime(n):
  if n in [2,3,5,7]:
    return True
  if n % 2 == 0:
    return False
  r = 3
  while r * r <= n:
    if n % r == 0:
      return False
    r += 2
  return True

def is_truncatableprime(n):
  truncated = [list(islice(str(n),i)) for i in range(1,len(str(n)))] + [list(islice(reversed(list(str(n))),i)) for i in range(1,len(str(n)))]
  truncatables = [int(''.join(truncated[i])) for i in range(len(truncated))]
  prime_truncatables = [truncatables[i] for i in range(len(truncated)) if is_prime(truncatables[i]) == True]
  return truncatables == prime_truncatables
  
def sum_of_prime_truncatables(LIMIT):
  return sum([i for i in range(10,LIMIT) if is_truncatableprime(i) == True][:10])

print(sum_of_prime_truncatable(10000))
