def factorial(n):
  fact = 1
  for i in range(1,n+1):
    fact = fact * i
  return [int(i) for i in str(fact)]

def Factorial_trailing_digits(n):
  digits = [x for x in factorial(n) if x != 0 ]
  return ''.join(str(i) for i in digits[-5:])

print(Factorial_trailing_digits(20))
