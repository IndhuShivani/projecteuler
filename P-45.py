def triangular(LIMIT):
  return set([n*(n+1)//2 for n in range(LIMIT)])
def pentagonal(LIMIT):
  return set([n*(3*n-1)//2 for n in range(LIMIT)])
def hexagonal(LIMIT):
  return set([n*(2*n-1) for n in range(LIMIT)])
def is_common(LIMIT):
  return triangular(LIMIT).intersection(pentagonal(LIMIT),hexagonal(LIMIT))
print(is_common(100000))
