def distinct_powers(LIMIT):
  return len(sorted({a ** b for a in range(2,LIMIT+1) for b in range(2,LIMIT+1)}))
