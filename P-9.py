def pythagorean_triplet():
  return set(a*b*c for a in range(2,1000) for b in range(2,1000) for c in range(2,1000) if a * a + b * b == c * c and a + b + c == 1000)
print(pythagorean_triplet())
