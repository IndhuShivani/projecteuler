from itertools import permutations as npr
def permuted(n):
  x = set([int("".join(i)) for i in list(npr(str(n),len(list(str(n)))))])
  y = set([i*n for i in range(2,7)])
  return x.intersection(y) == y
def permuted_multiples(LIMIT):
  return min([n for n in range(LIMIT) if permuted(n) == True])
print(permuted_multiples(1000000000))
