def sum_of_multiples(LIMIT):
  return sum(filter(lambda x: x % 3 == 0 or x % 5 == 0 ,range(LIMIT)))
print(sum_of_multiples(1000))
