from itertools import permutations as p
def permutations(n):
  return [int(''.join(i)) for i in list(p(str(n),len(list(str(n)))))]
def permuted_multiples(LIMIT):
  return [n for n in range(2,LIMIT) if 2*n in permutations(n) and 3*n in permutations(n) and 4*n in permutations(n) and 5*n in permutations(n) and 6*n in permutations(n)]
print(permuted_multiples(1000000))
