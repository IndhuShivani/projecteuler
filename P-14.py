def getcollatzsequence(x):
  collatz = []
  if(x < 0):
    return collatz
  else:
    collatz.append(x)
    while(x > 1):
      if(x % 2 == 0):
        x = x//2
        collatz.append(x)
      else:
        x = x * 3 + 1
        collatz.append(x)
  return collatz
def longest_collatz_sequence(LIMIT):
  return [i for i in range(LIMIT) if len(getcollatzsequence(i)) == max([len(getcollatzsequence(i)) for i in range(LIMIT)])]
print(longest_collatz_sequence(1000))
