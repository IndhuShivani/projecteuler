from math import pow
def num_of_ones(n):
  c = 0
  while n != 1:
    if n % 7 == 0:
      n = n // 7
    else:
      n = n + 1
      c = c + 1
  return c
def sum_of_nums(k):
  N = int( (pow(7,k) - 1) / 11)
  return sum([num_of_ones(n) for n in range(N)])
print(sum_of_nums(10))
