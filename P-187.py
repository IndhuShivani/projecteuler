def is_prime(n):
  if n in [2,3,5,7]:
    return True
  if n % 2 == 0:
    return False
  r = 3
  while r * r <= n:
    if n % r == 0:
      return False
    r += 2
  return True

def prime_factors(n):
    prime_factors = []
    r = 2
    while r*r <= n:
      while (n % r) == 0:
        prime_factors.append(r)
        n //= r
      r += 1
    if n > 1:
       prime_factors.append(n)
    return prime_factors

def semi_prime(LIMIT):
  return [n for n in range(2,LIMIT) if is_prime(n) == False and len(prime_factors(n)) == 2]

print(len(semi_prime(30)))
