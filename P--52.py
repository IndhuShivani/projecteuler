def permuted_multiples(LIMIT):
  return [n for n in range(2,LIMIT) if sorted(list(str(n))) == sorted(list(str(2*n))) == sorted(list(str(3*n))) == sorted(list(str(4*n))) == sorted(list(str(5*n))) == sorted(list(str(6*n)))]
print(permuted_multiples(1000000))
