from math import sqrt
def rotate(n):
  x = list(str(n))
  return str(x[-1])+str("".join(x[0:len(x)-1]))
def number_rotations(LIMIT):
  return sum([n for n in range(10,LIMIT) if int(rotate(n)) % n == 0])
print(number_rotations(int(pow(10,100))))
