from math import factorial as f
def ncr(n,r):
  return f(n)/(f(r)*f(n-r))
def combinatoric_selections():
  return len([ncr(n,r) for n in range(23,101) for r in range(4,n-3) if ncr(n,r) > 1000000])
print(combinatoric_selections())
