def factorial(n):
  fact = 1
  for i in range(1,n+1):
    fact *= i
  return fact

def is_curious(n):
  return n == sum([factorial(i) for i in [int(x) for x in str(n)]])

def curious_list(LIMIT):
  return [i for i in range(1,LIMIT) if is_curious(i) == True]
  
print(curious_list(100000))
