def is_nth_power(n):
  return n == sum([int(x)**len(list(str(n))) for x in str(n)])
def digit_nth_power(N):
  return sum([n for n in range(int(pow(10,N-1)),int(pow(10,N))) if is_nth_power(n) == True])
print(digit_nth_power(4))
